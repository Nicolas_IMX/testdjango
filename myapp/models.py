from django.db import models
from django.contrib import admin

# Create your models here.

class Employee(models.Model):  
    name = models.CharField(max_length=100)  
    email = models.EmailField()  
    contact = models.CharField(max_length=15) 
  
    class Meta:  
        db_table = "employee"

class Entreprises(models.Model):
    nameEnterprise = models.CharField(max_length=100)
    Email = models.EmailField()
    typeEntreprise = models.CharField(max_length=50)

    class Meta:
        db_table = "entreprises" 

class SuiviContrat(models.Model):
    dateDebut = models.DateField()
    dateFin = models.DateField()
    Poste = models.CharField(max_length=50, default="Développeur")
    EmployeeName = models.ForeignKey(Employee, on_delete=models.CASCADE)
    NewContrat = models.ForeignKey(Entreprises, on_delete=models.CASCADE)

    class Meta:
        db_table = "suivi_contrat"