from django.shortcuts import render, redirect  
from myapp.forms import EmployeeForm,EntreprisesForm,SuiviContratForm
from myapp.models import Employee, SuiviContrat,Entreprises

# Create your views here.

def addnew(request):  
    if request.method == "POST":  
        form = EmployeeForm(request.POST)  
        if form.is_valid():  
            try:
                form.save()  
                return redirect('/')  
            except:  
                pass 
    else:  
        form = EmployeeForm()
    suivicontrats = SuiviContrat.objects.all()
    return render(request,'index.html',{'form':form, 'suivicontrats': suivicontrats}) 

def index(request):
    employees = Employee.objects.all()
    suivicontrats = SuiviContrat.objects.all()
    return render(request,"show.html",{'employees':employees, 'suivicontrats': suivicontrats})

def indexSuivi(request):
    suivicontrats = SuiviContrat.objects.all()
    return render (request,"showSuivi.html",{'suivicontrats': suivicontrats})

def edit(request, id):  
    employee = Employee.objects.get(id=id)  
    return render(request,'edit.html', {'employee':employee}) 

def update(request, id):  
    employee = Employee.objects.get(id=id)  
    form = EmployeeForm(request.POST, instance = employee)  
    if form.is_valid():  
        form.save()
        return redirect("/")  
    return render(request, 'edit.html', {'employee': employee})

def destroy(request, id):  
    employee = Employee.objects.get(id=id)
    employee.delete()
    return redirect("/")

def addnewEnter(request):  
    if request.method == "POST":  
        form = EntreprisesForm(request.POST)  
        if form.is_valid():  
            try:
                form.save()  
                return redirect("/indexEnter")  
            except:  
                pass 
    else:  
        form = EntreprisesForm()
    return render(request,'indexEnter.html',{'form':form})

def indexEnter(request):  
    enterprises = Entreprises.objects.all()
    return render(request,"showEnter.html",{'enterprises':enterprises})

def editEnter(request, id):  
    enterprise = Entreprises.objects.get(id=id)  
    return render(request,'editEnter.html', {'enterprise':enterprise})

def updateEnter(request, id):  
    enterprise = Entreprises.objects.get(id=id)  
    form = EntreprisesForm(request.POST, instance = enterprise)  
    if form.is_valid():  
        form.save()  
        return redirect("/indexEnter")
    return render(request, 'editEnter.html', {'enterprise': enterprise})

def destroyEnter(request, id):  
    enterprise = Entreprises.objects.get(id=id)
    enterprise.delete()
    return redirect("/indexEnter")