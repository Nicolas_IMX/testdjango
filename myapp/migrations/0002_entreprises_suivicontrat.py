# Generated by Django 3.1.1 on 2020-09-04 08:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Entreprises',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nameEnterprise', models.CharField(max_length=100)),
                ('Email', models.EmailField(max_length=254)),
                ('typeEntreprise', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'entreprises',
            },
        ),
        migrations.CreateModel(
            name='SuiviContrat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dateDebut', models.IntegerField()),
                ('dateFin', models.IntegerField()),
                ('EmployeeName', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.employee')),
                ('NewContrat', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.entreprises')),
            ],
            options={
                'db_table': 'SuiviContrat',
            },
        ),
    ]
