# Generated by Django 3.1.1 on 2020-09-28 10:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0004_auto_20200928_1024'),
    ]

    operations = [
        migrations.AddField(
            model_name='suivicontrat',
            name='Poste',
            field=models.CharField(default='Développeur', max_length=50),
        ),
    ]
