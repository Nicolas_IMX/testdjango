from django import forms  
from myapp.models import Employee, Entreprises, SuiviContrat

class EmployeeForm(forms.ModelForm):  
    class Meta:  
        model = Employee  
        fields = ['name', 'contact', 'email']
        widgets = { 'name': forms.TextInput(attrs={ 'class': 'form-control' }), 
            'email': forms.EmailInput(attrs={ 'class': 'form-control' }),
            'contact': forms.TextInput(attrs={ 'class': 'form-control' }),
            }

class EntreprisesForm(forms.ModelForm):  
    class Meta:
        model = Entreprises  
        fields = ['nameEnterprise', 'typeEntreprise', 'Email']
        widgets = { 'nameEnterprise': forms.TextInput(attrs={ 'class': 'form-control' }), 
            'Email': forms.EmailInput(attrs={ 'class': 'form-control' }),
            'typeEntreprise': forms.TextInput(attrs={ 'class': 'form-control' }),
            }

class SuiviContratForm(forms.ModelForm):  
    class Meta:  
        model = SuiviContrat  
        fields = ['dateDebut', 'dateFin','EmployeeName', 'NewContrat']
        widgets = { 
            'dateDebut': forms.DateInput(attrs={ 'class': 'form-control' }), 
            'dateFin': forms.DateInput(attrs={ 'class': 'form-control' }),
            #'EmployeeName': forms.IntegerField(attrs={ 'class': 'form-control' }),
            #'NewContrat': forms.IntegerField(attrs={ 'class': 'form-control' })
        }